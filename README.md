# annotationSummary

This project explore several strategies for summarizing the annotations associated to a set of elements.

See also: https://gitlab.com/odameron/enrichmentAnalysisGO


# References

- Urgent need for consistent standards in functional enrichment analysis
	- wijesooriya2022consistentStandardsFunctionalEnrichmentAnalysis_35263338.pdf
	- https://pubmed.ncbi.nlm.nih.gov/35263338
- Heading down the wrong pathway: on the influence of correlation within gene sets
	- gatti2010correlationWithinGeneSets_20955544.pdf
	- https://pubmed.ncbi.nlm.nih.gov/20955544
- Functional modules, structural topology, and optimal activity in metabolic networks
	- resendis2012functionalModulesMetabolicNetworks_23071431.pdf
	- https://pubmed.ncbi.nlm.nih.gov/23071431
- REVIGO summarizes and visualizes long lists of gene ontology terms
	- supek2011revigo_21789182.pdf
	- https://pubmed.ncbi.nlm.nih.gov/21789182
- CirGO: an alternative circular way of visualising gene ontology terms
	- kuznetsova2019CirGOvisualisingGOterms_30777018.pdf
	- https://pubmed.ncbi.nlm.nih.gov/30777018
	- extension REVIGO
- Ontology-based information content computation
	- sanchez2011ontologyBasedInformationContent.pdf
	- https://www.sciencedirect.com/science/article/pii/S0950705110001619
- GO-function: deriving biologically relevant functions from statistically significant functions
	- wang2011goFunction_21705405.pdf
	- https://pubmed.ncbi.nlm.nih.gov/21705405
- Nine quick tips for pathway enrichment analysis
	- Davide Chicco, Giuseppe Agapito
	- PLoS Comput Biol. 2022 Aug 11;18(8):e1010348. doi: 10.1371/journal.pcbi.1010348.
	- https://pubmed.ncbi.nlm.nih.gov/35951505/
- How to learn about gene function: text-mining or ontologies?
	- Theodoros G Soldatos, Nelson Perdigão, Nigel P Brown, Kenneth S Sabir, Seán I O'Donoghue
	- Methods. 2015 Mar:74:3-15. doi: 10.1016/j.ymeth.2014.07.004.
	- https://pubmed.ncbi.nlm.nih.gov/25088781/
- Gene Set Summarization using Large Language Models
	- Marcin P Joachimiak, J Harry Caufield, Nomi L Harris, Hyeongsik Kim, Christopher J Mungall
	- ArXiv. 2023 May 25:arXiv:2305.13338v2. Preprint 
	- https://pubmed.ncbi.nlm.nih.gov/37292480/
- GO2Sum: generating human-readable functional summary of proteins from GO terms
	- Swagarika Jaharlal Giri , Nabil Ibtehaz , Daisuke Kihara
	- giri2024go2sumHumanReadableFunctionalSummary_38491038.pdf
	- https://pubmed.ncbi.nlm.nih.gov/38491038/

